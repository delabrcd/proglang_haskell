module TestQ5 (run,stmt,evalStmt) where
import Debug.Trace
import Data.Char
import Text.ParserCombinators.ReadP

data Statement = IfElse Condition Statement Statement |
                 While Condition Statement |
                 Assign Expression Expression |
                 Block [Statement] |
                 Declare Expression
  deriving Show

data Condition = Less Expression Expression |
                 Greater Expression Expression |
                 LessEq Expression Expression |
                 GreaterEq Expression Expression |
                 Equal Expression Expression |
                 NotEqual Expression Expression |
                 And Condition Condition |
                 Or Condition Condition |
                 Not Condition
  deriving Show

data Expression = Plus Expression Expression |
                   Minus Expression Expression |
                   Times Expression Expression |
                   Divide Expression Expression |
                   Var String |
                   Num Int
  deriving Show

type Memory = [(String,Int)]

parse :: ReadP a -> String -> a
parse p s
  | null parses = error "There are no parses"
  | length parses > 1 = error "There is more than one parse"
  | otherwise = head parses 
    where parses = map fst (filter (null . snd) (readP_to_S p s))
    
parsews :: ReadP a -> String -> a
parsews p s = parse p (filter (not . isSpace ) s)

run :: String -> Memory
run str = evalStmt (parsews stmt str) [] 

evalStmt :: Statement -> Memory -> Memory 
evalStmt (Block []) mem = mem
evalStmt (Block (stm:stms)) mem = evalStmt (Block stms) (evalStmt stm mem)
evalStmt (IfElse cnd blk1 blk2) mem 
  | (evalCond cnd mem) = (evalStmt blk1 mem)
  | otherwise = (evalStmt blk2 mem)
evalStmt (While cnd blk) mem
  | (evalCond cnd mem) = evalStmt (While cnd blk) (evalStmt blk mem)
  | otherwise = mem
evalStmt (Assign (Var e1) e2) mem = [(e1, (evalExp e2 mem))] ++ mem
evalStmt (Declare (Var e1)) mem = [(e1,0)] ++ mem

evalCond :: Condition -> Memory -> Bool
evalCond (Not c1) mem = not (evalCond c1 mem)
evalCond (And c1 c2) mem = (evalCond c1 mem) && (evalCond c2 mem)
evalCond (Or c1 c2) mem = (evalCond c1 mem) || (evalCond c2 mem)
evalCond (Less e1 e2) mem = (evalExp e1 mem) < (evalExp e2 mem)
evalCond (Greater e1 e2) mem = (evalExp e1 mem) > (evalExp e2 mem)
evalCond (LessEq e1 e2) mem = (evalExp e1 mem) <= (evalExp e2 mem)
evalCond (GreaterEq e1 e2) mem = (evalExp e1 mem) >= (evalExp e2 mem)
evalCond (Equal e1 e2) mem = (evalExp e1 mem) == (evalExp e2 mem)
evalCond (NotEqual e1 e2) mem = (evalExp e1 mem) /= (evalExp e2 mem)

makeCond :: String -> Expression -> Expression -> Condition
makeCond "<" e1 e2 = Less e1 e2
makeCond ">" e1 e2 = Greater e1 e2
makeCond "<=" e1 e2 = LessEq e1 e2
makeCond ">=" e1 e2 = GreaterEq e1 e2
makeCond "==" e1 e2 = Equal e1 e2
makeCond "!=" e1 e2 = NotEqual e1 e2 

evalExp :: Expression -> Memory -> Int
evalExp (Num n) _ = n
evalExp (Plus e1 e2) mem = evalExp e1 mem + evalExp e2 mem
evalExp (Minus e1 e2) mem = evalExp e1 mem  - evalExp e2 mem
evalExp (Times e1 e2) mem = evalExp e1 mem * evalExp e2 mem
evalExp (Divide e1 e2) mem = div (evalExp e1 mem) (evalExp e2 mem)
evalExp (Var v) mem 
  | answer == Nothing     = error $ v ++ " is not assigned"
  | otherwise = val
    where Just val = lookup v mem
          
stmt :: ReadP Statement
stmt = block +++ ifelse +++ while +++ variable +++ assign

block = do
  char '{' 
  stm <- many stmt
  char '}'
  return (Block stm)

ifelse = do
  string "if"
  char '('
  cnd <- cond
  char ')'
  blk1 <- block
  string "else"
  blk2 <- block
  return (IfElse cnd blk1 blk2)

while = do
  string "while("
  cnd <- cond
  string ")"
  blk <- block
  return (While cnd blk)

variable = do
  string "int"
  nm <- expr
  char ';'
  return (Declare nm)


assign = do
  e1 <- expr
  char '='
  e2 <- expr
  char ';'
  return (Assign e1 e2)


cond :: ReadP Condition
cond = ccomp +++ cnot +++ cand +++ cor

ccomp = do
  e1 <- expr
  op <- string ">" <++ string "<" <++ string ">=" <++ string "<=" <++ string "==" <++ string "!="
  e2 <- expr
  return (makeCond op e1 e2)

cbase = ccomp +++ cnot

cnot = do
  op <- char '!'
  c1 <- cond
  return (Not c1)

cand = do
  c1 <- cbase
  op <- string "&&"
  c2 <- cbase
  return (And c1 c2)

cor = do
  c1 <- cbase
  op <- string "||"
  c2 <- cbase
  return (Or c1 c2)

expr :: ReadP Expression
expr = chainl1 factor plus

plus :: ReadP (Expression -> Expression -> Expression)
plus = do
  op <- char '+' <++ char '-'
  return $ makeExpr op

factor :: ReadP Expression
factor = chainl1 base times

times :: ReadP (Expression -> Expression -> Expression)
times = do 
  op <- char '*' <++ char '/' 
  return $ makeExpr op

base :: ReadP Expression
base = num +++ var +++ parens 

parens :: ReadP Expression
parens = do
  char '('
  e <- expr
  char ')'
  return $ e

num :: ReadP Expression
num = do
  n <- munch1 isDigit
  return $ Num (read n :: Int)
  
var :: ReadP Expression
var = do 
  v <- munch1 isAlpha
  return $ Var v
 
makeExpr :: Char -> Expression -> Expression -> Expression
makeExpr '+' e1 e2 = Plus e1 e2
makeExpr '-' e1 e2 = Minus e1 e2
makeExpr '*' e1 e2 = Times e1 e2
makeExpr '/' e1 e2 = Divide e1 e2