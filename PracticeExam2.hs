module PracticeExam (remove, threeN, prefix, sublist, removeAll, singleton, breaklist, minusPairs) where
    -- 1
    remove :: Eq a => [a] -> a -> [a]
    remove [] _ = []
    remove (x:wholelist) elem
        | x == elem = wholelist
        | otherwise = [x] ++ remove wholelist elem
    -- 2
    threeN :: [Integer] -> [Integer]
    threeN intList 
        | elem (head intList) (tail intList) = intList
        | n `mod` 2 == 0 = threeN( [n `div` 2] ++ intList )
        | otherwise = threeN([3*n-1] ++ intList)
        where
            n = head intList
    
    -- 3
    prefix :: Eq a => [a] -> [a] -> Bool
    prefix [] _ = True
    prefix _ [] = False
    prefix (x:xs) (y:ys) = x == y && prefix xs ys
    
    -- 4
    sublist :: Eq a => [a] -> [a] -> Bool
    sublist xs (y:ys)
        | prefix xs (y:ys) = True
        | otherwise = sublist xs ys

    -- 5
    removeAll :: Eq a => [a] -> a -> [a]
    removeAll xs elem =
        [x | x <- xs, x /= elem]

    -- 6
    singleton :: [a] -> [[a]]
    singleton a = [[x] | x <- a]

    -- 7 
    type Pair = [(Integer, Integer)]

    breaklist :: [(Integer, [Integer])] -> Pair
    breaklist xs =
        concat([ [(fst x,y) | y <- snd x ] | x <- xs ])

    -- 8 

    minusPairs :: Pair -> [Integer]
    minusPairs xs =
        [snd x - fst x | x <- xs, fst x < snd x]
